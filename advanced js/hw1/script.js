"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    
    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    } 
    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary;
    }
}

// console.log(Employee);

class Programmer extends Employee {
    constructor (name, age, salary , lang) {
        super(name, age);
        this.salary = salary;
        this.lang = lang;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return `Current salary: ${this._salary * 3}$`;
    }

}

// console.log(Programmer);

const dev1 = new Programmer("Anastasiia", 24, 700, "English/Korean");
const dev2 = new Programmer("Oleksii", 28, 200, "English/German" );
const dev3 = new Programmer("Ihor", 25, 1000, "English/Russian" );


console.log(dev1);
console.log(dev1.salary);
console.log(dev2);
console.log(dev2.salary);
console.log(dev3);
console.log(dev3.salary);



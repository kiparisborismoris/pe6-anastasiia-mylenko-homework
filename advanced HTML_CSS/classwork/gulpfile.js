const { series, parallel, watch, src, dest } = require('gulp');
const browserSync = require("browser-sync").create();
const sass = require('gulp-sass')(require('sass'));

// gulp.series(), gulp.src(), gulp.dest()


// gulp.task("default", function (cb) {
//     console.log("default");
//     cb()
// });

// gulp.task("css", function (cb) {
//     console.log("defaucss donelt");
//     cb()
// }); // callback function required by gulp 

const serv = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
    });
};

const styles = (cb) => {
    // console.log("css done");
    // cb();
    return src("./src/scss/styles.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(dest("./dist/styles/"))
    .pipe(browserSync.stream());

}
// const html = (cb) => {
//     console.log("html done");
//     cb();
// }
const bsReload = (cb) => {
    browserSync.reload();
    cb();
};

const watcher = (cb) => {
    watch("./index.html", bsReload);
    watch("./src/scss/styles.scss", styles)
    cb();
}

exports.default = parallel(serv, watcher, series(styles));  //series(css, js, images)
// exports.css = css;
// exports  это объект существующий в нод джс и через него можно передавать данные

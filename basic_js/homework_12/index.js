// #Теоретический вопрос

//1.) setTimeout - вызывает функцию только один раз спустя заданное время, а setInterval вызывает функцию регулярно.
//2.) Если в функцию setTimeout() передать нулевую задержку, она выведется мгновенно, но только после выполнения основного кода.
//3.) setInterval продолжает вызывать функцию на регулярной основе после заданного интервала, поэтому важно остановить функцию чтобы не перенагружать систему.


const img = document.querySelector(".images-wrapper");
const stopBtn = document.createElement("button"); 
stopBtn.textContent = 'Прекратить';
img.after(stopBtn);

const pushBtn = document.createElement("button");
pushBtn.textContent = 'Возобновить показ';
stopBtn.after(pushBtn);


stopBtn.style.cssText=`margin:20px auto;`;
img.style.visibility="hidden";


console.log(img.children);

stopBtn.addEventListener("click", () => {
    clearInterval(timer);
} )

pushBtn.addEventListener("click", () => {
   timer = setInterval(viewImages, 3000);
})


let i =  0;

function viewImages() {
    if (i === img.children.length) {
        i = 0;
        img.children[i].style.visibility="visible"; 
        img.children[img.children.length -1].style.visibility = "hidden"; 

        console.log("Done"); } 
        
        else {
            img.children[i].style.visibility="visible"; 
           if (i > 0) {           
            img.children[i - 1].style.visibility="hidden";
            
        };
            i++;
        }
      
        console.log(img.children.length);
    }
    

 let timer = setInterval(viewImages, 3000);
  

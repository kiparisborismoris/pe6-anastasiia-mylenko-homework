"use strict";


const tabNavs = document.querySelectorAll('.tabs-title');
const tabPanes = document.querySelectorAll('.text');

for (let i = 0; i < tabNavs.length; i++) {
    tabNavs[i].addEventListener("click", function(e) {
        e.preventDefault();
        let activeTabAttr = e.target.getAttribute("data-tab");

        for (let y = 0; y < tabNavs.length; y++) {
            let contentAttr = tabPanes[y].getAttribute("data-tab-content");

            if(activeTabAttr === contentAttr) {
                tabNavs[y].classList.add("active");
                tabPanes[y].classList.add("active");
                tabPanes[y].style.display='block';

            } else {
                tabNavs[y].classList.remove("active");
                tabPanes[y].classList.remove("active");
                tabPanes[y].style.display='none';
            }            
        }
    });
}


// array.forEach(element => {
    
// target
// getAttribute
// начало страницы
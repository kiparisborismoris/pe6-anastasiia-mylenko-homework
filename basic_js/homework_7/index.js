"use strict";

// Document Object Model (DOM) - это всё содержимое страницы представленное в виде объектов. С помощью него можно менять элементы\стили страницы на лету, что делает её динамической.

let mass = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


function showList ( arr, parent) {
  let fragment = document.createDocumentFragment();
     let ulList = document.createElement("ul");
     fragment.append(ulList);


     let newArr = arr.map( (item) => {
       let liList = document.createElement("li");
       liList.textContent = item; 
      ulList.append(liList);
       
     });
     console.log(ulList);

     if (parent === undefined) {
       parent = document.body;
     };
     parent.append(fragment);

    //  ulList.innerHTML = newArr.join("");
    //  return ulList; 
}

// document.querySelector("body").after(showList(mass));

// for (let i = 1; i <= 5; i++) {
// let ulList = document.createElement("ul");
// ulList.textContent = "blablabla " + i;
// document.querySelector("body").append(ulList);
// }




showList(mass, document.querySelector("#root"));
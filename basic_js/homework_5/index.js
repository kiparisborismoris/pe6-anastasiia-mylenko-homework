//Теория 

// Экранирование в программировании нужно для обозначения специальных символов в коде (которые без экранирования могли бы читаться как часть кода или не выводится).
//  Поэтому перед такими символам добавляют "\" что и является экранированием.



"use strict"; 

function createNewUser(name, surname, birthday) {
    name = prompt("What's your name?");
    surname = prompt("What's your surname?");
    birthday = prompt("When's your birthday? (YYYY-MM-DD)");
    
return new Object({
    firstname: name,
    lastname: surname,
    date: birthday,
    getLogin() {
        return this.firstname[0].toLowerCase() + this.lastname.toLowerCase();
        },
    getAge(yy, mm, dd) {
         yy = this.date[0] + this.date[1] + this.date[2] + this.date[3];
         mm = this.date[5] + this.date[6];
         dd = this.date[8] + this.date[9];
        let otherDate = Math.floor((new Date() - Date.parse(this.date)) / (1000 * 60 * 60 * 24 * 365));
         return otherDate.toString();
         },    
    getPassword() {
        return this.firstname[0].toUpperCase() + this.lastname.toLowerCase() + (this.date[0] + this.date[1] + this.date[2] + this.date[3]);
    }    
    })
}

let newUser = createNewUser();
console.log(newUser.getLogin()); 
console.log(newUser.getAge());
console.log(newUser.getPassword());
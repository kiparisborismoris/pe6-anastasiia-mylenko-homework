"use strict";

const tabNavs = document.querySelectorAll('.service_list');
const tabContent = document.querySelectorAll('.services_chapter');

// tabs

tabNavs.forEach(function(el){
    el.addEventListener('click', function(e) {  
        e.preventDefault();
        let activeTabAttr = e.target.getAttribute('chapter-tab');

        for (let x = 0; x < tabNavs.length; x++) {
            let contentAttr = tabContent[x].getAttribute('chapter-tab-content');

            if(activeTabAttr === contentAttr) {
                tabNavs[x].classList.add("active");
                tabContent[x].classList.add("active");
                tabContent[x].style.display='flex';
            } else {
                tabNavs[x].classList.remove("active");
                tabContent[x].classList.remove("active");
                tabContent[x].style.display='none';
            }
            
        }})
})

// filter

const filterBox = document.querySelectorAll('.amazWork_backtitle')

document.querySelector('.nav2_menu').addEventListener('click', (event) => {
    event.preventDefault();

    let filterClass = event.target.parentNode.dataset['f'];
    
    // console.log(event.target.parentNode.dataset["f"]);
    console.log(filterClass);

    filterBox.forEach(elem => {
        elem.classList.remove('hide');
       
        if (!elem.classList.contains(filterClass) && filterClass !== 'all') {
            elem.classList.add('hide');
        }
    });
});



// button Load More

const loadMoreBtn = document.querySelector('.load_more');
const addContent = document.getElementsByClassName('btn_additional');


loadMoreBtn.addEventListener('click', function(u) {
    u.preventDefault();

    for (let z = 0; z < addContent.length; z++) {
        addContent[z].style.display = `block`;
        loadMoreBtn.style.visibility  = 'hidden';
    }
})

// slider

$(document).ready(function() {
    let position = 0;
    const slidesToShow = 4;
    const slidesToScroll = 1;
    const container = $('.slider-container');
    const track = $('.slider-track');
    const item = $('.slider-item');
    const btnPrev = $('.btn-prev');
    const btnNext = $('.btn-next');
    const itemsCount = item.length;
    const itemWidth = container.width() / slidesToShow;
    const movePosition = slidesToScroll * itemWidth;

    item.each(function(index, item) {
        $(item).css({
            minWidth: itemWidth,
        });
    });

    btnNext.click(function() { 
        const itemsLeft = itemsCount - (Math.abs(position) + slidesToShow * itemWidth) / itemWidth;
        
        position -= itemsLeft >= slidesToShow ? movePosition : itemsLeft * itemWidth;
        console.log(position);
        
        setPositioin();
        checkBtns();

    });

    btnPrev.click(function(){
        const itemsLeft = Math.abs(position) / itemWidth

        position += itemsLeft >= slidesToShow ? movePosition : itemsLeft * itemWidth;
        setPositioin();
        checkBtns();
        console.log(position);

    });

const setPositioin = () => {
    track.css({
        transform: `translateX(${position}px)`
    });
};

const checkBtns = () => {
    btnPrev.prop('disabled', position === 0);
    btnNext.prop(
        'disabled',
        position <= -(itemsCount - slidesToShow) * itemWidth);
};

checkBtns();
});

